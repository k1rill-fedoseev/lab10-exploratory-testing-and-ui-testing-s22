package com.kfedoseev.sqr;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestTiktok {
    WebDriver driver;
    private final String baseURL = "https://tiktok.com";

    @BeforeClass
    public void setUp() throws MalformedURLException {
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--headless");
        String webdriverHost = System.getenv("WEBDRIVER_HOST");
        if (webdriverHost == null || webdriverHost.isEmpty()) {
            driver = new FirefoxDriver(options);
        } else {
            driver = new RemoteWebDriver(new URL(webdriverHost), options);
        }
        driver.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testTikTokContinuousScroll() throws InterruptedException {
        driver.get(baseURL);

        System.out.println("Page Title is " + driver.getTitle());

        int videoCount1 = driver.findElements(By.xpath("//div[@data-e2e='feed-video']")).size();
        System.out.println("Initial video count is " +  videoCount1);

        Assert.assertTrue(videoCount1 > 1);

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,10000)");

        int videoCount2 = driver.findElements(By.xpath("//div[@data-e2e='feed-video']")).size();
        System.out.println("New video count is " +  videoCount2);

        Assert.assertTrue(videoCount2 > videoCount1);
    }
}